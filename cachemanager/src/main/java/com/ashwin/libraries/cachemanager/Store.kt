package com.ashwin.libraries.cachemanager

/**
 * Created by Ashwin on 6/2/2018.
 */

enum class Store {
    FILE,
    CACHE
}
