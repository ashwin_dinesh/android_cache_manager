package com.ashwin.libraries.cachemanager

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import org.json.JSONArray
import org.json.JSONObject
import java.io.*
import java.nio.charset.Charset

/**
 * Created by Ashwin on 6/2/2018.
 */

class CacheManager {

    companion object {

        @JvmStatic
        private fun getFile(applicationContext: Context, store: Store, directory: String?, filename: String?): File {
            val file = if (directory != null)  {
                if (store == Store.FILE) {
                    if (filename != null)
                        File(File(applicationContext.filesDir, directory), filename)
                    else
                        File(applicationContext.filesDir, directory)
                } else {
                    if (filename != null)
                        File(File(applicationContext.cacheDir, directory), filename)
                    else
                        File(applicationContext.cacheDir, directory)
                }
            } else {
                if (store == Store.FILE) {
                    if (filename != null)
                        File(applicationContext.filesDir, filename)
                    else
                        applicationContext.filesDir
                } else {
                    if (filename != null)
                        File(applicationContext.cacheDir, filename)
                    else
                        applicationContext.cacheDir
                }
            }

            return file
        }

        // json object
        @JvmStatic
        fun save(context: Context, store: Store, filename: String, content: JSONObject): Boolean {
            return save(context, store, null, filename, content)
        }

        @JvmStatic
        fun save(context: Context, store: Store, directory: String?, filename: String, content: JSONObject): Boolean {
            val strContent = content.toString()
            return save(context, store, filename, strContent)
        }

        @JvmStatic
        fun getJsonObject(context: Context, store: Store, filename: String): JSONObject? {
            return getJsonObject(context, store, null, filename)
        }

        @JvmStatic
        fun getJsonObject(context: Context, store: Store, directory: String?, filename: String): JSONObject? {
            val strContent = get(context, store, directory, filename) ?: return null
            return JSONObject(strContent)
        }

        // json array
        @JvmStatic
        fun save(context: Context, store: Store, filename: String, content: JSONArray): Boolean {
            return save(context, store, null, filename, content)
        }

        @JvmStatic
        fun save(context: Context, store: Store, directory: String?, filename: String, content: JSONArray): Boolean {
            val strContent = content.toString()
            return save(context, store, directory, filename, strContent)
        }

        @JvmStatic
        fun getJsonArray(context: Context, store: Store, filename: String): JSONArray? {
            return getJsonArray(context, store, null, filename)
        }

        @JvmStatic
        fun getJsonArray(context: Context, store: Store, directory: String?, filename: String): JSONArray? {
            val strContent = get(context, store, directory, filename) ?: return null
            return JSONArray(strContent)
        }

        // bitmap
        @JvmStatic
        fun save(context: Context, store: Store, filename: String, bitmap: Bitmap): Boolean {
            return save(context, store, null, filename, bitmap)
        }

        @JvmStatic
        fun save(context: Context, store: Store, directory: String?, filename: String, bitmap: Bitmap): Boolean {
            val file = getFile(context.applicationContext, store, directory, filename)
            if (file.exists())
                file.delete()

            var fileOutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
            fileOutputStream.close()
            return true
        }

        @JvmStatic
        fun getBitmap(context: Context, store: Store, filename: String): Bitmap? {
            return getBitmap(context, store, null, filename)
        }

        @JvmStatic
        fun getBitmap(context: Context, store: Store, directory: String?, filename: String): Bitmap? {
            val file = getFile(context.applicationContext, store, directory, filename)
            if (!file.exists())
                return null

            var bitmap: Bitmap? = BitmapFactory.decodeFile(file.absolutePath)

            if (bitmap == null) {
                val inputStream = file.inputStream()
                val jpegClosedInputStream = JpegClosedInputStream(inputStream)
                bitmap = BitmapFactory.decodeStream(jpegClosedInputStream)
                inputStream.close()
            }

            return bitmap
        }

        // string
        @JvmStatic
        fun save(context: Context, store: Store, filename: String, content: String): Boolean {
            return save(context, store, null, filename, content)
        }

        @JvmStatic
        fun save(context: Context, store: Store, directory: String?, filename: String, content: String): Boolean {
            val file = getFile(context.applicationContext, store, directory, filename)
            if (file.exists())
                file.delete()

            val fileOutputStream = FileOutputStream(file)
            val outputStreamWriter = OutputStreamWriter(fileOutputStream)
            outputStreamWriter.append(content)
            outputStreamWriter.close()

            return true
        }

        @JvmStatic
        fun get(context: Context, store: Store, filename: String): String? {
            return get(context, store, null, filename)
        }

        @JvmStatic
        fun get(context: Context, store: Store, directory: String?, filename: String): String? {
            val file = getFile(context.applicationContext, store, directory, filename)
            if (!file.exists())
                return null

            val inputStream = file.inputStream()
            val content = inputStream.readBytes().toString(Charset.defaultCharset())
            inputStream.close()
            return content
        }

        @JvmStatic
        fun list(context: Context, store: Store): List<String> {
            return list(context, store, null)
        }

        @JvmStatic
        fun list(context: Context, store: Store, directory: String?): List<String> {
            val parent = getFile(context.applicationContext, store, directory, null)

            var filesList: MutableList<String> = arrayListOf()
            val filesArray = parent.listFiles()
            for (file in filesArray) {
                filesList.add(file.name)
            }

            return filesList
        }

        @JvmStatic
        fun delete(context: Context, store: Store): Boolean {
            return delete(context, store, null)
        }

        @JvmStatic
        fun delete(context: Context, store: Store, filename: String?): Boolean {
            return delete(context, store, null, filename)
        }

        @JvmStatic
        fun delete(context: Context, store: Store, directory: String?, filename: String?): Boolean {
            val file = getFile(context.applicationContext, store, directory, filename)
            return file.delete()
        }
    }

    internal class JpegClosedInputStream constructor(private val inputStream: InputStream): InputStream() {
        private val JPEG_EOI_1 = 0xFF
        private val JPEG_EOI_2 = 0xD9
        private var bytesPastEnd: Int = 0

        init {
            bytesPastEnd = 0
        }

        @Throws(IOException::class)
        override fun read(): Int {
            var buffer = inputStream.read()
            if (buffer == -1) {
                if (bytesPastEnd > 0) {
                    buffer = JPEG_EOI_2
                } else {
                    ++bytesPastEnd
                    buffer = JPEG_EOI_1
                }
            }

            return buffer
        }
    }

}
